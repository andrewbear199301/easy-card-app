<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InitRewordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rewards')->insert([
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '至統一超商，消費滿100元 送10元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '至全家便利商店，消費滿100元 送10元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '至萊爾富，消費滿100元送 10元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '至ＯＫ便利商店，消費滿100元 送10元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '至85度C，消費滿100元 送10元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '至CoCo都可，消費滿100元 送10元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '至清心，消費滿100元 送10元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '至美廉社，消費滿150元 送15元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '至誠品，消費滿150元 送15元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '至三商巧福，消費滿200元 送20元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '至鬍鬚張，消費滿200元 送20元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '悠遊付全通路，消費滿100元 送10元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '悠遊付全通路，消費滿100元 送15元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '悠遊付全通路，消費滿100元 送20元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '悠遊付全通路，消費滿200元 送30元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '悠遊付全通路，消費滿200元 送40元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '悠遊付全通路，消費滿200元 送50元', 'qty' => 3503],
            ['no' => 'Ticket22061806440167', 'test_no' => 'Ticket22061806440167', 'name' => '悠遊付全通路，消費滿100元 送50元', 'qty' => 5859],
        ]);
    }
}
