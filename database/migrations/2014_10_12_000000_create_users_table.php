<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable()->comment('暱稱');
            $table->string('token')->nullable()->comment('悠遊卡token');
            $table->bigInteger('member_id')->nullable()->comment('悠遊卡member_id');
            $table->unsignedInteger('lap')->default(0)->comment('圈數');
            $table->unsignedInteger('point')->default(0)->comment('能量分數');
            $table->unsignedInteger('position')->default(1)->comment('格子位置');
            $table->boolean('can_answer')->default(0)->comment('是否可回答冷知識');
            $table->unsignedInteger('dice_qty')->default(2)->comment('骰子數量');
            $table->decimal('remainder', 7, 2)->default(0)->comment('換骰子餘數');
            $table->boolean('foreign')->default(0)->comment('是否為外國人');
            $table->boolean('avatar')->default(1)->comment('頭像');
            $table->unsignedInteger('map_card_id')->nullable()->comment('地圖植物');
            $table->boolean('check_achievement')->default(1)->comment('新成就');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
