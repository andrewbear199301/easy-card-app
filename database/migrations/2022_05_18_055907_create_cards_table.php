<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained()->nullOnDelete();
            $table->string('name')->nullable()->comment('名稱');
//            $table->string('type')->nullable()->comment('屬性');
            $table->string('card_surface_id')->unique()->comment('明碼');
            $table->string('card_physical_id')->nullable()->comment('隱碼');
            $table->json('before_data')->nullable()->comment('活動前詳細數據');
            $table->json('plant_data')->nullable()->comment('植物數據');
            $table->string('before_first_tra')->nullable()->comment('活動前第一名');
            $table->string('before_second_tra')->nullable()->comment('活動前第二名');
            $table->integer('before_count')->default(0)->comment('活動前總消費次數');
            $table->integer('after_count')->default(0)->comment('活動後總消費次數');
            $table->integer('after_distance')->default(0)->comment('活動後總距離');
            $table->decimal('before_reduction', 14, 3)->default(0)->comment('活動前總減碳量');
            $table->integer('before_distance')->default(0)->comment('活動前總距離');
            $table->unsignedInteger('point')->default(0)->comment('能量');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
};
