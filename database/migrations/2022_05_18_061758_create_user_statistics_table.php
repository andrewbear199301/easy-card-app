<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_statistics', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained()->nullOnDelete();
            $table->decimal('tra1_reduction', 14, 3)->default(0)->comment('公車');
            $table->decimal('tra2_reduction', 14, 3)->default(0)->comment('台鐵');
            $table->decimal('tra3_reduction', 14, 3)->default(0)->comment('捷運');
            $table->decimal('tra4_reduction', 14, 3)->default(0)->comment('腳踏車');
            $table->decimal('tra5_reduction', 14, 3)->default(0)->comment('高鐵');
            $table->unsignedInteger('mer1_count')->default(0)->comment('美廉社');
            $table->unsignedInteger('mer2_count')->default(0)->comment('全家');
            $table->unsignedInteger('mer3_count')->default(0)->comment('7-11');
            $table->unsignedInteger('mer4_count')->default(0)->comment('萊爾富');
            $table->unsignedInteger('mer5_count')->default(0)->comment('ok');
            $table->boolean('needed_update')->default(1)->comment('0:無須更新/1:須更新');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_statistics');
    }
};
