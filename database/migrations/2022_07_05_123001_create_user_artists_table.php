<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_artists', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained()->nullOnDelete();
            $table->boolean('artist_1')->default(0)->comment('藝術家格子 0:未踩/1:已踩');
            $table->boolean('artist_2')->default(0);
            $table->boolean('artist_3')->default(0);
            $table->boolean('artist_4')->default(0);
            $table->boolean('artist_5')->default(0);
            $table->boolean('artist_6')->default(0);
            $table->boolean('artist_7')->default(0);
            $table->boolean('artist_8')->default(0);
            $table->boolean('artist_9')->default(0);
            $table->boolean('artist_10')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_artists');
    }
};
