<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_achievements', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained()->nullOnDelete();
            $table->boolean('achievement_1')->default(0)->comment('藝術家成就 0:未達成/1:可領取/2:已領取');
            $table->boolean('achievement_2')->default(0)->comment('大安森林公園成就 0:未達成/1:可領取/2:已領取');
            $table->boolean('achievement_3')->default(0)->comment('bebe生日成就 0:未達成/1:可領取/2:已領取');
            $table->boolean('achievement_4')->default(0)->comment('減碳成就 0:未達成/1:可領取/2:已領取');
            $table->boolean('achievement_5')->default(0)->comment('消費成就 0:未達成/1:可領取/2:已領取');
            $table->boolean('achievement_6')->default(0)->comment('綁卡成就 0:未達成/1:可領取/2:已領取');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_achievements');
    }
};
