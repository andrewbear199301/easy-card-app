<?php

namespace App\Http\Controllers;

use App\Models\Reward;
use App\Services\RewardServices;
use App\Supports\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Dice
 *
 *
 */
class DiceController extends Controller
{
    protected $rewardService;

    public function __construct(RewardServices $rewardService)
    {
        $this->rewardService = $rewardService;
    }

//    public function test()
//    {
//        $reward = Reward::find(1);
//        $user = User::find(9);
//        $this->rewardService->exchangeReward($reward, $user);
//    }

    /**
     *
     * @authenticated
     *
     */
    public function roll(Request $request): object
    {
        $user = $request->user();
        if ($user->dice_qty <= 0) {
            return Response::fail('骰子不足');
        }
        $position_before = $user->position;
        $dice_num = rand(1, 6);
        $position_after = $user->position + $dice_num;
        $new_lap = false;
        $new_lap_reward = null;
        $title = null;
        $message = null;
        $reward = null;
        $move = null;
        $quiz = null;
        $sub_id = null;

        DB::beginTransaction();

        $user->decrement('dice_qty', 1);

        if ($position_after > 60) {
            $position_after = $position_after - 60;
            $user->increment('lap', 1);
            $new_lap = true;
            $new_lap_reward = $this->finishLap($user);
        }

        $position_type = config('game.map.' . $position_after . '.type');

        switch ($position_type) {
            case 1:
                $reward = $this->chanceCard($user);
                $title = '機會';
                $sub_id = $reward['sub_id'];
                $message = $reward['message'];
                break;
            case 2:
                $chest = $this->chestCard($user);
                $title = '命運';
                $message = $chest['message'];
                $move = $chest['move'];
                $sub_id = $chest['sub_id'];
                $position_after += $move;
                break;
            case 3:
                $reward = $this->enterpriseCard($user, $position_after);
                $title = '經過' . config('game.map.' . $position_after . '.content');
                $message = '玩場拉霸機，試試好手氣！';
                break;
            case 4:
                $quiz = $this->quizCard();
                $title = '悠遊卡冷知識';
                $user->update(['can_answer' => 1]);
                break;
            case 5:
                $title = '路過踩點';
                $message = config('game.map.' . $position_after . '.content');
                $reward = $this->randomTool($user);
                $sub_id = $reward['sub_id'];
                break;
            case 6:
                $title = '藝術家卡';
                $message = config('game.map.' . $position_after . '.content');
                $artist_id = config('game.map.' . $position_after . '.artist_id');
                $user_artist = $user->artist;
                $achievement = $user->achievement;
                if ($user_artist->$artist_id == 0 && $achievement->achievement_1 == 0) {
                    $user_artist->update([$artist_id => 1]);
                    if (array_count_values($user_artist->toArray())[0] <= 5) {
                        $user->achievement->update(['achievement_1' => 2]);
                    }
                }
                $reward = $this->randomTool($user);
                $sub_id = config('game.map.' . $position_after . '.sub_id');
                break;
        }
        $user->update(['position' => $position_after]);

        DB::commit();
        $data = [
            'type_id' => $position_type,
            'sub_id' => $sub_id,
            'new_lap' => $new_lap,
            'new_lap_reward' => $new_lap_reward,
            'title' => $title,
            'message' => $message,
            'reward' => $reward,
            'move' => $move,
            'quiz' => $quiz,
            'dice_num' => $dice_num,
            'position_before' => $position_before,
            'position_after' => $position_after,
            'dice_qty' => (int)$user->dice_qty,
            'point' => (int)$user->point,
        ];

        return Response::ok($data);
    }

    private function chanceCard($user)
    {
        $reward_id = $this->drawChance();
        if ($reward_id) {
            $reward = Reward::find($reward_id);
            if (!$reward->isOutOfStack(3503, 113) && $this->rewardService->exchangeReward($reward, $user)) {
                $reward->exchange();
                $user->rewards()->attach($reward_id, ['type' => '機會格']);
                return [
                    'reward_type' => 1,
                    'title' => 'GET! 回饋券',
                    'message' => $reward->name,
                    'sub_id' => $reward->id,
                ];
            }
        }
        return $this->randomToolForChance($user);
    }

    private function drawChance()
    {
        $rand = (float)rand() / (float)getrandmax();
        if ($rand < config('game.chance_1')) return 12;
        if ($rand < config('game.chance_2')) return 13;
        if ($rand < config('game.chance_3')) return 14;
        if ($rand < config('game.chance_4')) return 15;
        if ($rand < config('game.chance_5')) return 16;
        if ($rand < config('game.chance_6')) return 17;
        return null;
    }

    private function chestCard($user)
    {
        $rand = rand(1, 5);
        $sub_id = null;
        switch ($rand) {
            case 1:
                $move = -1;
                $message = '下雨，寸步難行，退後一格';
                $sub_id = 1;
                break;
            case 2:
                $move = -3;
                $message = '突然來的地震！退後三格';
                $sub_id = 2;
                break;
            case 3:
                $move = null;
                $message = '好心人幫忙！獲得骰子x1';
                $user->addDice(1);
                $sub_id = 3;
                break;
            case 4:
                $move = +3;
                $message = '發現祕密通道，前進三格';
                $sub_id = 4;
                break;
            case 5:
                $move = +5;
                $message = '公車進站，順利搭上車，前進五格';
                $sub_id = 5;
                break;
            default:
                $move = null;
                $message = null;
        }
        return ['move' => $move, 'message' => $message, 'sub_id' => $sub_id];
    }

    private function finishLap($user)
    {
        $rand = (float)rand() / (float)getrandmax();
        if ($rand < config('game.finish_lap')) {
            $reward = Reward::find(18);
            if (!$reward->isOutOfStack(5859, 189) && $this->rewardService->exchangeReward($reward, $user)) {
                $reward->exchange();
                $user->rewards()->attach(18, ['type' => '環島一周']);
                return [
                    'reward_type' => 1,
                    'title' => 'GET! 回饋券',
                    'message' => $reward->name,
                ];
            }
        }
        return [
            'reward_type' => 0,
            'title' => '再接再厲',
            'message' => '繼續往前，有機會嬴得更多回饋券',
        ];
    }

    private function enterpriseCard($user, $position_after)
    {
        $reward_id = config('game.map.' . $position_after . '.reward_id');
        $rand = (float)rand() / (float)getrandmax();
        if ($rand < config('game.enterprise')) {
            $reward = Reward::find($reward_id);
            if (!$reward->isOutOfStack(3503, 113) && $this->rewardService->exchangeReward($reward, $user)) {
                $reward->exchange();
                $user->rewards()->attach($reward_id, ['type' => '拉霸機']);
                return [
                    'reward_type' => 1,
                    'title' => 'GET! 回饋券',
                    'message' => $reward->name,
                ];
            }
        }
        return [
            'reward_type' => 0,
            'title' => '再接再厲',
            'message' => '繼續往前，有機會嬴得更多回饋券',
        ];
    }

    private function quizCard()
    {
        $rand = rand(1, 10);
        return config('game.quizzes.' . $rand);
    }

    private function randomToolForChance($user)
    {
        $rand = rand(1, 3);
        $tool = config('game.chance_tools.' . $rand);
        $added_point = $user->addPointByTool($tool['point']);
        return [
            'reward_type' => 2,
            'title' => 'GET! ' . $tool['title'],
            'message' => $tool['message'],
            'added_point' => (int)$added_point,
            'sub_id' => $rand,
        ];
    }

    private function randomTool($user)
    {
        $rand = rand(1, 3);
        $tool = config('game.tools.' . $rand);
        $added_point = $user->addPointByTool($tool['point']);
        return [
            'reward_type' => 2,
            'title' => 'GET! ' . $tool['title'],
            'message' => $tool['message'],
            'added_point' => (int)$added_point,
            'sub_id' => $rand,
        ];
    }
}
