<?php

namespace App\Http\Controllers;

use App\Models\Card;
use App\Services\CardServices;
use App\Supports\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


/**
 * @group Card
 *
 *
 */
class CardController extends Controller
{
    protected $cardService;

    public function __construct(CardServices $cardService)
    {
        $this->cardService = $cardService;
    }

    public function storeByPhysical(Request $request)
    {
        $request->validate([
            'card_physical_id' => 'required|integer',
        ]);
        //TODO recaptcha
        if (!$card = Card::where('card_physical_id', $request->card_physical_id)->first()) {
            DB::beginTransaction();
            $card = $this->cardService->createCardByPhysicalID($request->card_physical_id);
            DB::commit();
        }
        if(!$card){
            return Response::fail('無此卡片');
        }
        return Response::ok([
            'id' => $card->id,
            'name' => $card->name,
            'card_surface_id' => $card->card_surface_id,
            'before_first_tra' => $card->before_first_tra,
            'before_second_tra' => $card->before_second_tra,
            'before_reduction' => $card->before_reduction,
            'before_distance' => $card->before_distance,
            'plant_data' => $card->plant_data,
//            'type' => $card->type,
        ]);
    }

    /**
     *
     *
     * @bodyParam card_surface_id required integer
     *
     * @response {"data": {
     * "id": 8,
     * "card_surface_id": "9122017917570325",
     * "before_first_tra": "tra3_reduction",
     * "before_second_tra": "tra1_reduction",
     * "before_reduction": "0.00",
     * "before_distance": "3180.79",
     * "plant_data": {
     * "flex": -0.17,
     * "angle": 0.5,
     * "color": 0,
     * "texture": 14,
     * "leafType": 0,
     * "angleBias": 11.2,
     * "petalType": 3,
     * "curvesType": 5,
     * "stamenType": 5,
     * "stamenColor": 0,
     * "bendingDegree": 10,
     * "bendingSmooth": 1.9,
     * "textureRepeat": 2.9
     * }
     * }
     * }
     */
    public function store(Request $request): object
    {
        $request->validate([
            'card_surface_id' => 'required|integer',
        ]);
        //TODO recaptcha
        if (!$card = Card::where('card_surface_id', $request->card_surface_id)->first()) {
            DB::beginTransaction();
            $card = $this->cardService->createCardBySurfaceID($request->card_surface_id);
            DB::commit();
        }
        if(!$card){
            return Response::fail('無此卡片');
        }
        return Response::ok([
            'id' => $card->id,
            'name' => $card->name,
            'card_surface_id' => $card->card_surface_id,
            'before_first_tra' => $card->before_first_tra,
            'before_second_tra' => $card->before_second_tra,
            'before_reduction' => $card->before_reduction,
            'before_distance' => $card->before_distance,
            'plant_data' => $card->plant_data,
//            'type' => $card->type,
        ]);
    }

    /**
     *
     * @authenticated
     * @response {
     * "data": {
     * "map_card_id": 1,
     * "cards": [
     * {
     * "id": 1,
     * "name": null,
     * "before_first_tra": "tra2_reduction",
     * "before_second_tra": "tra3_reduction",
     * "after_distance": "0.00",
     * "after_count": 50,
     * "point": 52,
     * "before_reduction": "20.61",
     * "before_distance": "294.81",
     * "plant_data": null
     * },
     * {
     * "id": 3,
     * "name": null,
     * "before_first_tra": "tra2_reduction",
     * "before_second_tra": "tra3_reduction",
     * "after_distance": "5.00",
     * "after_count": 0,
     * "point": 52,
     * "before_reduction": "20.61",
     * "before_distance": "294.81",
     * "plant_data": null
     * },
     * {
     * "id": 4,
     * "name": null,
     * "before_first_tra": "tra3_reduction",
     * "before_second_tra": "tra1_reduction",
     * "after_distance": "3.58",
     * "after_count": 7,
     * "point": 0,
     * "before_reduction": "126.79",
     * "before_distance": "1838.31",
     * "plant_data": null
     * },
     * {
     * "id": 5,
     * "name": null,
     * "before_first_tra": "tra3_reduction",
     * "before_second_tra": "tra1_reduction",
     * "after_distance": "0.00",
     * "after_count": 0,
     * "point": 0,
     * "before_reduction": "0.00",
     * "before_distance": "1605.89",
     * "plant_data": {
     * "flex": -0.02,
     * "angle": 0.2,
     * "color": 0,
     * "texture": 12,
     * "leafType": 1,
     * "angleBias": 2,
     * "petalType": 7,
     * "curvesType": 5,
     * "stamenType": 6,
     * "stamenColor": 3,
     * "bendingDegree": -4,
     * "bendingSmooth": 1.8,
     * "textureRepeat": 2.9
     * }
     * }
     * ]
     * }
     * }
     */
    public function list(Request $request): object
    {
        $user = $request->user();
        $result = [];
        $cards = $user->cards()->get(['id', 'name', 'before_first_tra', 'before_second_tra', 'after_distance', 'after_count' , 'card_surface_id'
            , 'point', 'before_reduction', 'before_distance', 'plant_data']);
        foreach($cards as $card){
            $result[] = [
                'id' => $card->id,
                'name' => $card->name,
                'before_first_tra' => $card->before_first_tra,
                'before_second_tra' => $card->before_second_tra,
                'after_distance' => $card->after_distance,
                'after_count' => $card->after_count,
                'card_surface_id' => $card->card_surface_id,
                'point' => $card->point,
                'before_reduction' => $card->before_reduction * 1000,
                'before_distance' => $card->before_distance,
                'plant_data' => $card->plant_data,
            ];
        }
        return Response::ok(['map_card_id' => $user->getMapCardId(), 'cards' => $result]);
    }

    /**
     *
     * @authenticated
     * @bodyParam name string required
     * @bodyParam id integer required 卡片id
     *
     */
    public function updateName(Request $request): object
    {
        $request->validate([
            'name' => 'required|string',
        ]);
        $user = $request->user();
        $user->cards()->where('id', $request->id)->first()->update(['name' => $request->name]);

        return Response::ok();
    }

    /**
     *
     * @authenticated
     * @bodyParam id string card_id required
     *
     * @response status=400 {"message": "無此卡片"}
     */
    public function changeMapCard(Request $request): object
    {
        $request->validate([
            'id' => 'required|integer|exists:cards',
        ]);
        $user = $request->user();
        if ($user->cards()->where('id', $request->id)->exists()) {
            $user->update(['map_card_id' => $request->id]);
            return Response::ok();
        }
        return Response::fail('無此卡片');
    }
}
