<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Supports\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


/**
 * @group Leaderboard
 *
 *
 */
class LeaderboardController extends Controller
{
    /**
     *
     * @authenticated
     *
     * @response
     * {
     * "data": {
     * "leaderboard": [
     * {
     * "id": 2,
     * "name": "3443",
     * "point": 12
     * },
     * {
     * "id": 1,
     * "name": "dfgdfsgdfs;a",
     * "point": 2
     * }
     * ],
     * "rank": 2,
     * "lap": 2,
     * "point": 98
     * }
     * }
     */
    public function index(Request $request): object
    {
        $user = $request->user();
        $users = User::orderByDesc('point')->get(['id', 'name', 'point', 'avatar']);
        $rank = $users->where('id', $user->id)->keys()->first() + 1;
        $leaderboard = $this->leaderboardArray($users->take(10));
        $data = [
            'leaderboard' => $leaderboard,
            'rank' => $rank,
            'lap' => $user->lap,
            'point' => $user->point,
        ];
        return Response::ok($data);
    }

    private function leaderboardArray($users)
    {
        $leaderboard = [];
        foreach ($users as $user) {
            $leaderboard[] = [
                'id' => $user->id,
                'name' => $user->name,
                'avatar' => $user->avatar,
                'point' => $user->point,
            ];
        }
        return $leaderboard;
    }

//    public function saveImage(Request $request)
//    {
//        $src = md5(uniqid() . time()) . '.png';
//        Storage::disk('public')->put("sharer/$src", base64_decode($request->image));
//        return Response::ok();
//    }
}
