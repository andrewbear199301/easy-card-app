<?php

namespace App\Http\Controllers;

use App\Models\Card;
use App\Models\User;
use App\Services\CardServices;
use App\Supports\Response;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group User
 *
 *
 */
class UserController extends Controller
{
    protected $cardService;

    public function __construct(CardServices $cardService)
    {
        $this->cardService = $cardService;
    }


    /**
     *
     *
     * @bodyParam   token    string  required 先隨便填
     *
     * @responseField new_cards 是否有新卡 沒有則空陣列
     * @responseField new_user 是否為新user
     * @response {
     *"data": {
     * "new_cards": [
     * {
     * "id": 4,
     * "card_surface_id": "7393414253",
     * "before_first_tra": "tra3_reduction",
     * "before_second_tra": "tra1_reduction",
     * "before_reduction": "126.79",
     * "before_distance": "1838.31",
     * "plant_data": null
     * },
     * {
     * "id": 5,
     * "card_surface_id": "5339741415",
     * "before_first_tra": "tra3_reduction",
     * "before_second_tra": "tra1_reduction",
     * "before_reduction": "0.00",
     * "before_distance": "1605.89",
     * "plant_data": {
     * "flex": -0.02,
     * "angle": 0.2,
     * "color": 0,
     * "texture": 12,
     * "leafType": 1,
     * "angleBias": 2,
     * "petalType": 7,
     * "curvesType": 5,
     * "stamenType": 6,
     * "stamenColor": 3,
     * "bendingDegree": -4,
     * "bendingSmooth": 1.8,
     * "textureRepeat": 2.9
     * }
     * }
     * ],
     * "access_token": "9|tC8XFUCiUX0el4f4X5JDa0r3zDS9GOPtuV8P5YPg",
     * "token_type": "Bearer",
     * "new_user": false
     * }
     * }
     * @response status=400 {"message": "無記名卡"}
     *
     */
    public function login(Request $request): object
    {
        $request->validate([
            'token' => 'required|string',
        ]);
        $new_user = false;
        $token = $request->token;
        $new_cards = [];

        DB::beginTransaction();

        $before_data = $this->cardService->getBeforeDataByToken($token);
        if (!$before_data) {
            return Response::fail('無記名卡');
        }
        $card_list = $before_data['card_list'];
        $member_id = $before_data['member_id'];

        if (!$card_list) {
            return Response::fail('無記名卡');
        }

        $user = User::firstOrCreate(['member_id' => $member_id]);
//        $user = User::where('token' , $token)->first();


        if ($user->wasRecentlyCreated) {
            $new_user = true;
        }
        $updated_at = Carbon::parse($user->daily->updated_at);
        if (!$updated_at->isToday()) {
            $user->update(['check_achievement' => 1]);
        }
        $new_cards = $this->checkCardList($card_list, $user, $new_user);

        $access_token = $user->createToken('auth_token')->plainTextToken;
        $user->update(['token' => $token]);
        DB::commit();
        $data = [
            'new_cards' => $new_cards,
            'access_token' => $access_token,
            'token_type' => 'Bearer',
            'new_user' => $new_user,
        ];
        return Response::ok($data);
    }

    /**
     *
     * @authenticated
     *
     * @responseField dice_update 是否需更新骰子 沒有則空陣列
     * @responseField check_achievement 成就是否需提示
     * @responseField map_card 地圖植物
     * @responseField avatar 頭像 1~4
     *
     * @response{"data": {
     * "dice_update": {
     * "mer1_count": 0,
     * "mer2_count": 5,
     * "mer3_count": 0,
     * "mer4_count": 0,
     * "mer5_count": 0,
     * "tra1_reduction": 0,
     * "tra2_reduction": 0,
     * "tra3_reduction": 2.2113,
     * "tra4_reduction": 0,
     * "tra5_reduction": 0,
     * "added_dices": 12,
     * "start_date": 8/01,
     * "end_date": 8/05
     * },
     * "map_card": {
     * "before_reduction": "20.61",
     * "before_distance": "294.81",
     * "before_first_tra": "tra2_reduction",
     * "before_second_tra": "tra3_reduction",
     * "plant_data": {
     * "flex": -0.02,
     * "angle": 0.2,
     * "color": 0,
     * "texture": 12,
     * "leafType": 1,
     * "angleBias": 2,
     * "petalType": 7,
     * "curvesType": 5,
     * "stamenType": 6,
     * "stamenColor": 3,
     * "bendingDegree": -4,
     * "bendingSmooth": 1.8,
     * "textureRepeat": 2.9
     * }
     * },
     * "point": 52,
     *
     * "position": 16,
     * "lap": 2,
     * "point": 96,
     * "name": "test",
     * "dice_qty": 49,
     * "check_achievement": null
     * "avatar": 1
     * "birthday": false
     * }
     * }
     */
    public function index(Request $request): object
    {
        $user = $request->user();
        $token = $user->token;
        $dice_update = [];
        DB::beginTransaction();
        if ($user->isNeededUpdate()) {
            $dice_update = $this->cardService->getAfterDataByToken($token, $user);
            if (is_null($dice_update)) {
                return Response::fail('Something went wrong');
            }
            $user->statistic->update(['needed_update' => 0]);
        }
        DB::commit();
        $map_card = $user->cards()->where('id', $user->getMapCardId())->first();
        $data = [
            'dice_update' => $dice_update,
            'map_card' => [
                'before_reduction' => $map_card->before_reduction * 1000,
                'before_distance' => $map_card->before_distance,
                'before_first_tra' => $map_card->before_first_tra,
                'before_second_tra' => $map_card->before_second_tra,
                'after_count' => $map_card->after_count,
                'after_distance' => $map_card->after_distance,
                'plant_data' => $map_card->plant_data,
                'point' => $map_card->point,
            ],
            'position' => $user->position,
            'lap' => (int)$user->lap,
            'point' => (int)$user->point,
            'name' => $user->name,
            'dice_qty' => (int)$user->dice_qty,
            'check_achievement' => $user->check_achievement,
            'artists' => $user->artist,
            'avatar' => $user->avatar,
            'birthday' => Carbon::now()->day == 8,
        ];
        return Response::ok($data);
    }

    /**
     *
     * @authenticated
     *
     * @bodyParam name string required
     * @bodyParam avatar integer required 1~4
     *
     */

    public function updateName(Request $request): object
    {
        $request->validate([
            'name' => 'required',
            'avatar' => 'required|integer|between:1,4',
        ]);
        $user = $request->user();
        $user->update(['name' => $request->name, 'avatar' => $request->avatar]);

        return Response::ok();
    }

    private function checkCardList($card_list, $user, $new_user)
    {
        $new_cards = [];
        $diff_card_surface_ids = array_diff_assoc($card_list, $user->cards()->pluck('card_surface_id')->toArray());
        if (!empty($diff_card_surface_ids)) {
            foreach ($diff_card_surface_ids as $card_surface_id) {
                if (!$card = Card::where('card_surface_id', $card_surface_id)->first()) {
                    $card = $this->cardService->createCardBySurfaceID($card_surface_id);
                    if (!$card) {
                        continue;
                    }
                }
                $achievement = $user->achievement;
                if ($achievement->achievement_6 == 0 && !$new_user) {
                    $achievement->update(['achievement_6' => 1]);
                }
                $card->update(['user_id' => $user->id]);
                $new_cards[] = [
                    'id' => $card->id,
                    'card_surface_id' => $card->card_surface_id,
                    'before_first_tra' => $card->before_first_tra,
                    'before_second_tra' => $card->before_second_tra,
                    'before_reduction' => $card->before_reduction * 1000,
                    'before_distance' => $card->before_distance,
                    'plant_data' => $card->plant_data,
                ];
            }
            $user->statistic->update(['needed_update' => 1]);
        }
        return $new_cards;
    }
}
