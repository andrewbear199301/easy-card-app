<?php

namespace App\Http\Controllers;

use App\Supports\Response;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Daily
 *
 *
 */
class DailyController extends Controller
{
    /**
     *
     * @authenticated
     *
     * @responseField status 0:未達成 1:已達成未領取 2:已領取
     * @responseField is_exchanged 0:今日未兌換 1:已兌換 (要點了才算解完美日)
     *
     * @response{
     * "data": {
     * "consecutive": 1,
     * "achievements": [
     * {
     * "id": 1,
     * "status": 0,
     * "value": ""
     * },
     * {
     * "id": 2,
     * "status": 0,
     * "value": 0
     * },
     * {
     * "id": 3,
     * "status": 0,
     * "value": 0
     * },
     * {
     * "id": 4,
     * "status": 0,
     * "value": 8.58
     * },
     * {
     * "id": 5,
     * "status": 1,
     * "value": 0
     * },
     * {
     * "id": 6,
     * "status": 0,
     * "value": 0
     * }
     * ]
     * }
     * }
     */
    public function index(Request $request): object
    {
        $user = $request->user();
        $daily = $user->daily;
        $updated_at = Carbon::parse($daily->updated_at);
        $achievements = $this->achievementList($user);

        if ($updated_at->isToday()) {
            return Response::ok([
                'consecutive' => $daily->consecutive,
                'is_exchanged' => $daily->is_exchanged,
                'achievements' => $achievements,
            ]);
        }
        $user->update(['check_achievement' => 0]);
        $daily->update(['is_exchanged' => 0]);
        if ($updated_at->isYesterday()) {
            if ($daily->consecutive == 7) {
                $daily->touch();
                return Response::ok([
                    'consecutive' => $daily->consecutive,
                    'is_exchanged' => $daily->is_exchanged,
                    'achievements' => $achievements,
                ]);
            }
            $daily->increment('consecutive');
            return Response::ok([
                'consecutive' => $daily->consecutive,
                'is_exchanged' => $daily->is_exchanged,
                'achievements' => $achievements,
            ]);
        }
        $daily->update(['consecutive' => 1]);
        $daily->touch();


        return Response::ok([
            'consecutive' => $daily->consecutive,
            'is_exchanged' => $daily->is_exchanged,
            'achievements' => $achievements,
        ]);
    }

    /**
     *
     * @authenticated
     * @response {
     * "data": {
     * "consecutive": 1,
     * "added_point": 20
     * }
     * }
     *
     * @response status=400 {"message": "已兌換過"}
     */
    public function exchangeDaily(Request $request): object
    {
        $user = $request->user();
        $added_point = 0;
        $daily = $user->daily;
        if ($daily->is_exchanged) {
            return Response::fail('已兌換過');
        }
        $consecutive = $daily->consecutive;
        DB::beginTransaction();
        switch ($consecutive) {
            case 1:
                $added_point = $user->addPointByTool(2);
                break;
            case 2:
                $added_point = $user->addPointByTool(3);
                break;
            case 3:
                $added_point = $user->addPointByTool(5);
                break;
            case 4:
                $user->addPoint(15);
                $added_point = 15;
                break;
            case 5:
                $user->addDice(1);
                break;
            case 6:
                $user->addPoint(20);
                $added_point = 20;
                break;
            case 7:
                $user->addDice(2);
                break;
            default:
                break;
        }
        $user->daily()->update(['is_exchanged' => 1]);
        DB::commit();
        return Response::ok(['consecutive' => $consecutive, 'added_point' => (int)$added_point]);
    }

    /**
     *
     * @authenticated
     * @bodyParam id integer required achievement_id
     *
     * @response {
     * "data": {
     * "point": 50
     * },
     * @response status=400 {"message": "兌換失敗"}
     */
    public function exchangeAchievement(Request $request): object
    {
        $user = $request->user();
        $id = $request->id;
        $achievement_id = 'achievement_' . $id;
        $achievement = $user->achievement;
        if ($achievement->$achievement_id == 1) {
            $point = 0;
            DB::beginTransaction();
            switch ($id) {
                case 1:
                default:
                    break;
                case 2:
                case 4:
                    $point = 50;
                    $user->addPoint($point);
                    break;
                case 3:
                    $point = 8;
                    $user->addPoint($point);
                    break;
                case 5:
                case 6:
                    $point = 30;
                    $user->addPoint($point);
                    break;
            }
            $achievement->update([$achievement_id => 2]);
            DB::commit();
            return Response::ok(['point' => $point]);
        }
        return Response::fail('兌換失敗');
    }


    /**
     *
     * @authenticated
     *
     * @response status=400 {"message": "失敗"}
     */
    public function birthday(Request $request): object
    {
        $user = $request->user();
        $achievement_id = 'achievement_3';
        $achievement = $user->achievement;
        $dt = Carbon::now();
        if ($dt->day !== 8 || $achievement->$achievement_id !== 0) {
            return Response::fail('失敗');
        }
        $achievement->update([$achievement_id => 1]);
        return Response::ok();
    }

    private function achievementList($user)
    {
        $achievement = $user->achievement;
        $achievement_1_value = 0;
        $achievement_2_value = 0;
        $achievement_3_value = 0;
        $achievement_4_value = 0;
        $achievement_5_value = 0;
        $achievement_6_value = 0;
        if ($achievement->achievement_1 == 0) {
            $achievement_1_value = 10 - (array_count_values($user->artist->toArray())[0] ?? 0);
        }
        if ($achievement->achievement_4 == 0) {
            $after_distance = $user->cards->sum('after_distance');
            if ($after_distance >= 150) {
                $achievement->update(['achievement_4' => 1]);
            } else {
                $achievement_4_value = $after_distance;
            }
        }
        if ($achievement->achievement_5 == 0) {
            $after_count = $user->cards->sum('after_count');
            if ($after_count >= 20) {
                $achievement->update(['achievement_5' => 1]);
            } else {
                $achievement_5_value = $after_count;
            }
        }

        return [
            [
                'id' => 1,
                'status' => $achievement->achievement_1,
                'value' => $achievement_1_value,
            ],
            [
                'id' => 2,
                'status' => $achievement->achievement_2,
                'value' => $achievement_2_value,
            ],
            [
                'id' => 3,
                'status' => $achievement->achievement_3,
                'value' => $achievement_3_value,
            ],
            [
                'id' => 4,
                'status' => $achievement->achievement_4,
                'value' => $achievement_4_value,
            ],
            [
                'id' => 5,
                'status' => $achievement->achievement_5,
                'value' => $achievement_5_value,
            ],
            [
                'id' => 6,
                'status' => $achievement->achievement_6,
                'value' => $achievement_6_value,
            ],
        ];

    }
}
