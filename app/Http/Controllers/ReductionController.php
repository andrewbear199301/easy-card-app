<?php

namespace App\Http\Controllers;

use App\Supports\Response;
use Illuminate\Http\Request;


/**
 * @group Reduction
 *
 *
 */
class ReductionController extends Controller
{
    /**
     *
     * @authenticated
     *
     * @response
     * {
     * "data": [
     * {
     * "tra2_reduction": {
     * "value": "34526.00",
     * "percentage": "72%"
     * }
     * },
     * {
     * "tra4_reduction": {
     * "value": "8675.00",
     * "percentage": "18%"
     * }
     * },
     * {
     * "tra3_reduction": {
     * "value": "4321.00",
     * "percentage": "9%"
     * }
     * },
     * {
     * "tra1_reduction": {
     * "value": "76.00",
     * "percentage": "0%"
     * }
     * },
     * {
     * "tra5_reduction": {
     * "value": "55.00",
     * "percentage": "0%"
     * }
     * }
     * ]
     * }
     */
    public function index(Request $request): object
    {
        $user = $request->user();
        $statistic = $user->statistic;
        $data = [];
        $reduction_arr = [
            'tra1_reduction' => $statistic->tra1_reduction * 1000,
            'tra2_reduction' => $statistic->tra2_reduction * 1000,
            'tra3_reduction' => $statistic->tra3_reduction * 1000,
            'tra4_reduction' => $statistic->tra4_reduction * 1000,
            'tra5_reduction' => $statistic->tra5_reduction * 1000,
        ];
        $total = array_sum($reduction_arr);
        arsort($reduction_arr);
        foreach ($reduction_arr as $key => $value) {
            $data[] = [
                $key => [
                    'value' => $value,
                    'percentage' => number_format($value / $total * 100) . '%',
                ]
            ];
        }

        return Response::ok($data);
    }
}
