<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Services\CardServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $cardService;

    public function __construct(CardServices $cardService)
    {
        $this->cardService = $cardService;
    }

    public function login(Request $request)
    {
        $new_user = false;
//        $token = $request->token;
//
//        $this->cardService->

        //TODO 悠遊卡 api
//        if (!Auth::attempt($request->only('email', 'password'))) {
//            return response()->json([
//                'message' => 'Invalid login details'
//            ], 401);
//        }

//        $user = User::firstOrFail();
        $user = User::firstOrCreate();
        if ($user->wasRecentlyCreated) {
            $new_user = true;
        }
        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'new_user' => $new_user,
        ]);
    }


}
