<?php

namespace App\Http\Controllers;

use App\Supports\Response;
use Illuminate\Http\Request;


/**
 * @group Reward
 *
 *
 */
class RewardController extends Controller
{
    /**
     *
     * @authenticated
     *
     * @response
     * {
     * "data": [
     * {
     * "name": "消費滿200元回30元",
     * "type": "機會格",
     * "created_at": "2022-06-17"
     * },
     * {
     * "name": "消費滿100元回10元(統一)",
     * "type": "拉霸機:經過",
     * "created_at": "2022-06-17"
     * },
     * {
     * "name": "消費滿100元回50元",
     * "type": "環島一周",
     * "created_at": "2022-06-17"
     * }
     * ]
     * }
     */
    public function index(Request $request): object
    {
        $user = $request->user();
        $data = [];
        foreach ($user->rewards()->orderByDesc('reward_user.created_at')->get() as $reward) {
            $data[] = [
                'name' => $reward->name,
                'type' => $reward->pivot->type,
                'created_at' => $reward->pivot->created_at->format('Y-m-d'),
            ];
        }
        return Response::ok($data);
    }
}
