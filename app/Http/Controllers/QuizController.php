<?php

namespace App\Http\Controllers;

use App\Supports\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


/**
 * @group Quiz
 *
 *
 */
class QuizController extends Controller
{
    /**
     *
     * @authenticated
     * @bodyParam id integer required quiz_id
     * @bodyParam answer integer required answer_id
     *
     */
    public function answer(Request $request): object
    {
        $request->validate([
            'id' => 'required|integer',
            'answer' => 'required|integer',
        ]);
        $user = $request->user();
        if (!$user->can_answer) {
            return Response::fail('無法回答問題');
        }
        $quiz = config('game.quizzes.' . $request->id);
        $result = false;
        $sub_id = null;
        $reward = [];

        DB::beginTransaction();
        $user->update(['can_answer' => 0]);
        if ($quiz['answer'] == $request->answer) {
            $reward = $this->randomTool($user);
            $sub_id = $reward['sub_id'];
            $result = true;
        }
        DB::commit();

        return Response::ok([
            'result' => $result,
            'reward' => $reward,
            'sub_id' => $sub_id,
        ]);
    }


    private function randomTool($user)
    {
        $rand = rand(1, 3);
        $tool = config('game.tools.' . $rand);
        $added_point = $user->addPointByTool($tool['point']);
        return [
            'reward_type' => 2,
            'title' => 'GET! ' . $tool['title'],
            'message' => $tool['message'],
            'added_point' => $added_point,
            'sub_id' => $rand,
        ];
    }
}
