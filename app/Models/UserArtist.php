<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperUser
 */
class UserArtist extends Model
{
    protected $table = 'user_artists';

    protected $guarded = [
        'id'
    ];

}
