<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * @mixin IdeHelperUser
 */
class UserStatistic extends Model
{

    protected $table = 'user_statistics';


    protected $guarded = [
       'id'
    ];

}
