<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperUser
 */
class UserAchievement extends Model
{
    protected $table = 'user_achievements';

    protected $guarded = [
        'id'
    ];

}
