<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperCard
 */
class Card extends Model
{
    protected $table = 'cards';

    protected $guarded = ['id'];

    protected $casts = [
        'before_data' => 'array',
        'plant_data' => 'array',
    ];

//    protected static function booted()
//    {
//        static::created(function ($card) {
////            $name = '';
//            $arr = [
//                'tra1_reduction' => 5,
//                'tra2_reduction' => 1,
//                'tra3_reduction' => 0,
//                'tra4_reduction' => 2,
//                'tra5_reduction' => 3,
//            ];
//            if ($card->before_reduction = 0) {
//                $curvesType = 4;
//                $color = 4;
////                $name .= '';
//            } else {
//                $curvesType = $arr[$card->before_second_tra];
//                $color = $arr[$card->before_first_tra];
//            }
//            if ($curvesType == 3) {
//                $bendingDegree = 0;
//            } else {
//                $bendingDegree = mt_rand(-15, 15);
//            }
//
//            if ($curvesType == 0) {
//                $bendingSmooth = mt_rand(1, 2) / 10;
//            } elseif ($curvesType == 1) {
//                $bendingSmooth = 0.1;
//            } else {
//                $bendingSmooth = mt_rand(1, 20) / 10;
//            }
//            $angle = mt_rand(-2, 6) / 10;
//            $angleBias = mt_rand(7, 125) / 10;
//            $flex = mt_rand(-50, 15) / 100;
//            $leafType = mt_rand(0, 4);
//            $petalType = mt_rand(0, 9);
//            $arr2 = [
//                'tra1_reduction' => [3, 4, 5],
//                'tra2_reduction' => [6, 7, 8],
//                'tra3_reduction' => [12, 13, 14],
//                'tra4_reduction' => [0, 1, 2],
//                'tra5_reduction' => [9, 10, 11],
//                'mer1_count' => [27, 28, 29],
//                'mer2_count' => [18, 19, 20],
//                'mer3_count' => [15, 16, 17],
//                'mer4_count' => [21, 22, 23],
//                'mer5_count' => [24, 25, 26],
//            ];
//            if ($card->before_reduction = 0) {
//                $data = json_decode($card->before_data);
//                $mer = $this->shuffle_assoc($data['mer']);
//                arsort($mer);
//                $result = $arr2[array_keys($mer)[0]];
//                $texture = $result[mt_rand(0, 2)];
//            } else {
//                $result = $arr2[$card->before_first_tra];
//                $texture = $result[mt_rand(0, 2)];
//            }
//            $textureRepeat = mt_rand(15, 35) / 10;
//            $stamenType = mt_rand(0, 9);
//            $stamenColor = mt_rand(0, 4);
//
//            $plant_data = json_encode([
//                'curvesType' => $curvesType,
//                'color' => $color,
//                'bendingDegree' => $bendingDegree,
//                'bendingSmooth' => $bendingSmooth,
//                'angle' => $angle,
//                'angleBias' => $angleBias,
//                'flex' => $flex,
//                'leafType' => $leafType,
//                'petalType' => $petalType,
//                'texture' => $texture,
//                'textureRepeat' => $textureRepeat,
//                'stamenType' => $stamenType,
//                'stamenColor' => $stamenColor,
//            ]);
//
//
//
//            $card->update(['plant_data' => $plant_data]);
//        });
//    }

    private function shuffle_assoc($list)
    {
        if (!is_array($list)) return $list;

        $keys = array_keys($list);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $list[$key];
        }
        return $random;
    }

}
