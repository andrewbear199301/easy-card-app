<?php

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperCard
 */
class Reward extends Model
{
    protected $table = 'rewards';

    protected $guarded = ['id'];

    public function isOutOfStack($total, $per): bool
    {
        return $this->qty <= ($total - (Carbon::now()->day * $per));
    }

    public function exchange()
    {
        $this->decrement('qty');
    }
}
