<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperUser
 */
class UserDaily extends Model
{
    protected $table = 'user_dailies';

    protected $guarded = [
        'id'
    ];
//
//    public function refreshExchangeStatus(): bool
//    {
//        return $this->update(['is_exchanged' => 0]);
//    }

}
