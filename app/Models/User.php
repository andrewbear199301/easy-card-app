<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @mixin IdeHelperUser
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected static function booted()
    {
        static::created(function ($user) {
            $user->statistic()->create();
            $user->daily()->create();
            $user->achievement()->create();
            $user->artist()->create();
        });
    }

    protected $guarded = [
        'id'
    ];

    public function cards(): HasMany
    {
        return $this->hasMany(Card::class, 'user_id');
    }

    public function statistic(): HasOne
    {
        return $this->hasOne(UserStatistic::class, 'user_id');
    }

    public function daily(): HasOne
    {
        return $this->hasOne(UserDaily::class, 'user_id');
    }

    public function achievement(): HasOne
    {
        return $this->hasOne(UserAchievement::class, 'user_id');
    }

    public function artist(): HasOne
    {
        return $this->hasOne(UserArtist::class, 'user_id');
    }

    public function rewards(): BelongsToMany
    {
        return $this->belongsToMany(Reward::class)->withPivot('type')->withTimestamps();
    }

    public function addPointByTool($point)
    {
        $cards = $this->cards;
        foreach ($cards as $card) {
            $card->increment('point', $point);
        }
        $point_sum = $cards->count() * $point;
        $this->increment('point', $point_sum);
        return $point_sum;
    }

    public function addPoint($point)
    {
        $this->increment('point', $point);
    }

    public function addDice($dice)
    {
        $this->increment('dice_qty', $dice);
    }

    public function isNeededUpdate(): bool
    {
        return $this->statistic->needed_update || !Carbon::parse($this->statistic->updated_at)->isToday();
    }

    public function getMapCardId()
    {
        return $this->map_card_id ?: $this->cards()->first()->id;
    }

//    public function isDiceExceed(int $qty = 0): bool
//    {
//        return (bool)$this->dice_qty >= config('game.max_dices') - $qty;
//    }
}
