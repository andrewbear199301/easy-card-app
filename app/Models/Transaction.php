<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperCard
 */
class Transaction extends Model
{
    protected $table = 'transactions';

    protected $guarded = ['id'];
}
