<?php

namespace App\Services;

use App\Models\Transaction;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Spatie\ArrayToXml\ArrayToXml;

class RewardServices
{
    public function exchangeReward($reward, $user): bool
    {
        $dt = Carbon::now();
        $array = [
            'Trans' => [
                'T0100' => '0200',
                'T0200' => $user->member_id,
                'T0209' => ($dt->month == 8) ? $reward->no : $reward->test_no,
                'T0300' => '120010',
                'T1100' => str_pad(Transaction::count() + 1, 6, "0", STR_PAD_LEFT),
                'T1200' => $dt->format('His'),
                'T1300' => $dt->format('Ymd'),
                'T3700' => $dt->format('Ymd') . str_pad(Transaction::whereDate('created_at', $dt)->count() + 1, 7, "0", STR_PAD_LEFT),
                'T4100' => gethostname(),
                'T4200' => '53228645',
            ]
        ];
        $xml = ArrayToXml::convert($array, 'TransXML', true, 'UTF-8', '1.0', [], true);

        /*                $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><TransXML><Trans><T0100>0200</T0100><T0200>1202001200000002</T0200>*/
//<T0209>Ticket22061806440167</T0209><T0300>120010</T0300><T1100>000304</T1100><T1200>153529</T1200><T1300>20220511</T1300>
//<T3700>051115000304</T3700><T4100>server_name</T4100><T4200>123456789</T4200></Trans></TransXML>';
        $encrypted = $this->encrypt($xml);
        if ($encrypted) {
            $url = config('api.reward_api_url');
            $client = new Client(['verify' => false]);
            $response = $client->request('POST', $url,
                [
                    'headers' => [
                        'Content-Type' => 'application/xml',
                    ],
                    'body' => $encrypted]);
            $decrypted = $this->decrypt($response->getBody()->getContents());
            if ($decrypted) {
                Transaction::create(['data' => $decrypted]);
                $xml = simplexml_load_string($decrypted);
                if ((string)$xml->T3900[0] == '00') {
                    return true;
                }
            }
        }
        return false;
    }

    private function encrypt($string)
    {
        $key = config('api.encrypt_key');
        $hex = bin2hex(strtoupper($key));
        $iv = mb_substr($hex, 0, 16);
        $iv = strtoupper($iv);
        $cipher = "aes-256-cbc";
        if (in_array($cipher, openssl_get_cipher_methods())) {
            $ciphertext = openssl_encrypt($string, $cipher, $key, OPENSSL_RAW_DATA, $iv);
            return base64_encode(gzencode(strtoupper(bin2hex($ciphertext))));
        }
        return false;
    }

    private function decrypt($string)
    {
        $key = config('api.encrypt_key');
        $hex = bin2hex(strtoupper($key));
        $iv = mb_substr($hex, 0, 16);
        $iv = strtoupper($iv);
        $cipher = "aes-256-cbc";
        if (in_array($cipher, openssl_get_cipher_methods())) {
            $str = hex2bin(gzdecode(base64_decode($string)));
            return openssl_decrypt($str, $cipher, $key, OPENSSL_RAW_DATA, $iv);
        }
        return false;
    }
}
