<?php

namespace App\Services;


use App\Models\Card;
use Carbon\Carbon;
use GuzzleHttp\Client;

class CardServices
{
    public function createCardBySurfaceID($id)
    {
        $url = config('api.card_api_url');
        $client = new Client();
        $response = $client->request('GET', $url . '/GetDataBySurfaceID?cardID=' . $id,
            ['timeout' => 120]);
        $xml = simplexml_load_string(simplexml_load_string($response->getBody()->getContents()));
        $card_info = [
            'card_physical_id' => $xml->xpath('//str[@name="card_physical_id"]') ? (string)$xml->xpath('//str[@name="card_physical_id"]')[0] : 0,
            'card_surface_id' => $xml->xpath('//str[@name="card_surface_id"]') ? (string)$xml->xpath('//str[@name="card_surface_id"]')[0] : 0,
            'mer' => [
                'mer1_count' => $xml->xpath('//int[@name="mer1_count"]') ? (string)$xml->xpath('//int[@name="mer1_count"]')[0] : 0,
                'mer2_count' => $xml->xpath('//int[@name="mer2_count"]') ? (string)$xml->xpath('//int[@name="mer2_count"]')[0] : 0,
                'mer3_count' => $xml->xpath('//int[@name="mer3_count"]') ? (string)$xml->xpath('//int[@name="mer3_count"]')[0] : 0,
                'mer4_count' => $xml->xpath('//int[@name="mer4_count"]') ? (string)$xml->xpath('//int[@name="mer4_count"]')[0] : 0,
                'mer5_count' => $xml->xpath('//int[@name="mer5_count"]') ? (string)$xml->xpath('//int[@name="mer5_count"]')[0] : 0,
            ],
            'tra_reduction' => [
                'tra1_reduction' => $xml->xpath('//str[@name="tra1_reduction"]') ? (string)$xml->xpath('//str[@name="tra1_reduction"]')[0] : 0,
                'tra2_reduction' => $xml->xpath('//str[@name="tra2_reduction"]') ? (string)$xml->xpath('//str[@name="tra2_reduction"]')[0] : 0,
                'tra3_reduction' => $xml->xpath('//str[@name="tra3_reduction"]') ? (string)$xml->xpath('//str[@name="tra3_reduction"]')[0] : 0,
                'tra4_reduction' => $xml->xpath('//str[@name="tra4_reduction"]') ? (string)$xml->xpath('//str[@name="tra4_reduction"]')[0] : 0,
                'tra5_reduction' => $xml->xpath('//str[@name="tra5_reduction"]') ? (string)$xml->xpath('//str[@name="tra5_reduction"]')[0] : 0,
            ],
            'tra_distance' => [
                'tra1_distance' => $xml->xpath('//str[@name="tra1_distance"]') ? (string)$xml->xpath('//str[@name="tra1_distance"]')[0] : 0,
                'tra2_distance' => $xml->xpath('//str[@name="tra2_distance"]') ? (string)$xml->xpath('//str[@name="tra2_distance"]')[0] : 0,
                'tra3_distance' => $xml->xpath('//str[@name="tra3_distance"]') ? (string)$xml->xpath('//str[@name="tra3_distance"]')[0] : 0,
                'tra4_distance' => $xml->xpath('//str[@name="tra4_distance"]') ? (string)$xml->xpath('//str[@name="tra4_distance"]')[0] : 0,
                'tra5_distance' => $xml->xpath('//str[@name="tra5_distance"]') ? (string)$xml->xpath('//str[@name="tra5_distance"]')[0] : 0,
            ],
        ];
        if (!$card_info['card_physical_id']) {
            return false;
        }
        $tra_reduction = $this->shuffle_assoc($card_info['tra_reduction']);
        arsort($tra_reduction);

        $card = Card::create([
            'card_surface_id' => $card_info['card_surface_id'],
            'card_physical_id' => $card_info['card_physical_id'],
            'before_data' => json_encode($card_info),
            'before_first_tra' => array_keys($tra_reduction)[0],
            'before_second_tra' => array_keys($tra_reduction)[1],
            'before_reduction' => round(array_sum($tra_reduction), 3),
            'before_distance' => ceil(array_sum($card_info['tra_distance'])),
            'before_count' => array_sum($card_info['mer']),
        ]);

        $name = '';
//        $type = null;
        $arr = [
            'tra1_reduction' => 5,
            'tra2_reduction' => 1,
            'tra3_reduction' => 0,
            'tra4_reduction' => 2,
            'tra5_reduction' => 3,
        ];
        if ($card->before_reduction == 0) {
            $curvesType = 4;
            $color = 4;
        } else {
            $curvesType = $arr[$card->before_second_tra];
            $color = $arr[$card->before_first_tra];
        }
        if ($curvesType == 3) {
            $bendingDegree = 0;
        } else {
            $bendingDegree = mt_rand(-15, 15);
        }

        if ($curvesType == 0) {
            $bendingSmooth = mt_rand(1, 2) / 10;
        } elseif ($curvesType == 1) {
            $bendingSmooth = 0.1;
        } else {
            $bendingSmooth = mt_rand(1, 20) / 10;
        }
        $angle = mt_rand(-2, 6) / 10;
        $angleBias = mt_rand(7, 125) / 10;
        $flex = mt_rand(-50, 15) / 100;
        $leafType = mt_rand(0, 4);
        $petalType = mt_rand(0, 9);
        $arr2 = [
            'tra1_reduction' => [3, 4, 5],
            'tra2_reduction' => [6, 7, 8],
            'tra3_reduction' => [12, 13, 14],
            'tra4_reduction' => [0, 1, 2],
            'tra5_reduction' => [9, 10, 11],
            'mer1_count' => [27, 28, 29],
            'mer2_count' => [18, 19, 20],
            'mer3_count' => [15, 16, 17],
            'mer4_count' => [21, 22, 23],
            'mer5_count' => [24, 25, 26],
        ];
        $name_arr = [
            'tra1_reduction' => ['卍霸', '距向系', '刺球'],
            'tra2_reduction' => ['宿命√', '跨域系', '過山'],
            'tra3_reduction' => ['煞氣a', '時間系', '地下'],
            'tra4_reduction' => ['夢☆', '角力系', '魔豆'],
            'tra5_reduction' => ['Baby', '強化系', '閃蝶'],
            'mer1_count' => ['正義', '火焰系'],
            'mer2_count' => ['戀羽', '變化系'],
            'mer3_count' => ['天下', '特質系'],
            'mer4_count' => ['永恆', '原生系'],
            'mer5_count' => ['琥珀', '操作系'],
            'end' => ['花', '仙', '蕨']
        ];
        if ($card->before_reduction == 0) {
            $data = json_decode($card->before_data);
            $mer = $this->shuffle_assoc($data['mer']);
            arsort($mer);
            $result = $arr2[array_keys($mer)[0]];
            $texture = $result[mt_rand(0, 2)];
            $name .= $name_arr[array_keys($mer)[0]][0];
//            $type = $name_arr[array_keys($mer)[0]][1];
        } else {
            $result = $arr2[$card->before_first_tra];
            $texture = $result[mt_rand(0, 2)];
            $name .= $name_arr[$card->before_first_tra][0];
//            $type = $name_arr[$card->before_first_tra][1];
        }
        $name .= $name_arr[$card->before_second_tra][2];
        $name .= $name_arr['end'][mt_rand(0, 2)];
        $textureRepeat = mt_rand(15, 35) / 10;
        $stamenType = mt_rand(0, 9);
        $stamenColor = mt_rand(0, 4);

        $plant_data = json_encode([
            'curvesType' => $curvesType,
            'color' => $color,
            'bendingDegree' => $bendingDegree,
            'bendingSmooth' => $bendingSmooth,
            'angle' => $angle,
            'angleBias' => $angleBias,
            'flex' => $flex,
            'leafType' => $leafType,
            'petalType' => $petalType,
            'texture' => $texture,
            'textureRepeat' => $textureRepeat,
            'stamenType' => $stamenType,
            'stamenColor' => $stamenColor,
        ]);

        $card->update([
            'plant_data' => $plant_data,
            'name' => $name,
//            'type' => $type,
        ]);

        return $card;
    }

    public function getBeforeDataByToken($token)
    {
        $url = config('api.token_api_url');
        $client = new Client(['verify' => false]);
        $body = json_encode(['token' => $token]);
        $response = $client->request('POST', $url . '/getUserDataBeforeEventStart',
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ],
                'body' => $body]);
//        $response = '<reponse><merchant><doc><str name="member_id">1202110042969815</str><int name="mer2_amount">203</int><int name="mer2_count">5</int><int name="mer4_amount">32</int><int name="mer4_ count">1</int><int name="mer5 amount">175</int><int name="mer5_count">2</int><str name="trandate">BEFORE</str></doc> </merchant><transport><doc><str name="member_id">1202110042969815</str><str name="tral_distance">49.7409</str><str name="tral_reduction">2.8352</str> <str name="tra2_distance">100.9000</str><str name="tra2_reduction">7.4666</str><str name="tra3_distance">30.8500</str> <str name="tra3_reduction">2.2829</str><str name="trandate">BEFORE</str> </doc></transport><cardlist><card> <card_physical_id>2497779533</card_physical_id> <card_surface_id>9022190000006659</card_surface_id><name_card>Y</name_card> </card><card><card_physical_id>3741269588</card_physical_id> <card_surface_id>9022150000000040</card_surface_id><name_card>Y</name_card> </card><card><card_physical_id>2800362095</card_physical_id> <card_surface_id>9022190000014029</card_surface_id><name_card>Y</name_card> </card><card><card_physical_id>633504898</card_physical_id> <card_surface_id>9022150000000041</card_surface_id><name_card>Y</name_card> </card></cardlist></reponse>';
        $res = json_decode($response->getBody()->getContents());
        if (isset($res->error)) {
            return null;
        }
        $xml_string = $res->data;
        $xml = simplexml_load_string($xml_string);
        $member_id = (string)$xml->member_id;
        $card_list = [];
        if ($xml->cardlist) {
            foreach ($xml->cardlist->card as $card) {
                $card_list[] = (string)$card->card_surface_id;
            }
        }
//        dd($card_list);
        return [
            'member_id' => $member_id,
            'card_list' => $card_list
        ];
    }

    public function getAfterDataByToken($token, $user)
    {
        $url = config('api.token_api_url');
        $client = new Client(['verify' => false]);
        $body = json_encode(['token' => $token]);
        $response = $client->request('POST', $url . '/getUserDataAfterEventStart',
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ],
                'body' => $body]);
        $res = json_decode($response->getBody()->getContents());
        if (isset($res->error)) {
            return null;
        }
        $xml_string = $res->data;
        $xml = simplexml_load_string($xml_string);
        $exchange_data = [];
        if ($xml->merchant) {
            foreach ($xml->merchant->doc as $card) {
                $card_surface_id = $card->xpath('str[@name="card_surface_id"]') ? (string)$card->xpath('str[@name="card_surface_id"]')[0] : 0;
                $mer1_count = $card->xpath('int[@name="mer1_count"]') ? (string)$card->xpath('int[@name="mer1_count"]')[0] : 0;
                $mer2_count = $card->xpath('int[@name="mer2_count"]') ? (string)$card->xpath('int[@name="mer2_count"]')[0] : 0;
                $mer3_count = $card->xpath('int[@name="mer3_count"]') ? (string)$card->xpath('int[@name="mer3_count"]')[0] : 0;
                $mer4_count = $card->xpath('int[@name="mer4_count"]') ? (string)$card->xpath('int[@name="mer4_count"]')[0] : 0;
                $mer5_count = $card->xpath('int[@name="mer5_count"]') ? (string)$card->xpath('int[@name="mer5_count"]')[0] : 0;

                if ($card_surface_id == 'ALL') {
                    $exchange_data['mer']['mer1_count'] = $mer1_count;
                    $exchange_data['mer']['mer2_count'] = $mer2_count;
                    $exchange_data['mer']['mer3_count'] = $mer3_count;
                    $exchange_data['mer']['mer4_count'] = $mer4_count;
                    $exchange_data['mer']['mer5_count'] = $mer5_count;
                } else {
                    $after_count = $mer1_count + $mer2_count + $mer3_count + $mer4_count + $mer5_count;
                    $user->cards()->where('card_surface_id', $card_surface_id)->first()->update(['after_count' => $after_count]);
                }
            }
        }

        if ($xml->transport) {
            foreach ($xml->transport->doc as $card) {
                $card_surface_id = (string)$card->xpath('str[@name="card_surface_id"]')[0];
                if ($card_surface_id == 'ALL') {
                    $exchange_data['tra']['tra1_reduction'] = $card->xpath('str[@name="tra1_reduction"]') ? (string)$card->xpath('str[@name="tra1_reduction"]')[0] : 0;
                    $exchange_data['tra']['tra2_reduction'] = $card->xpath('str[@name="tra2_reduction"]') ? (string)$card->xpath('str[@name="tra2_reduction"]')[0] : 0;
                    $exchange_data['tra']['tra3_reduction'] = $card->xpath('str[@name="tra3_reduction"]') ? (string)$card->xpath('str[@name="tra3_reduction"]')[0] : 0;
                    $exchange_data['tra']['tra4_reduction'] = $card->xpath('str[@name="tra4_reduction"]') ? (string)$card->xpath('str[@name="tra4_reduction"]')[0] : 0;
                    $exchange_data['tra']['tra5_reduction'] = $card->xpath('str[@name="tra5_reduction"]') ? (string)$card->xpath('str[@name="tra5_reduction"]')[0] : 0;

                } else {
                    $after_distance = 0;
                    $after_distance += $card->xpath('str[@name="tra1_distance"]') ? (string)$card->xpath('str[@name="tra1_distance"]')[0] : 0;
                    $after_distance += $card->xpath('str[@name="tra2_distance"]') ? (string)$card->xpath('str[@name="tra2_distance"]')[0] : 0;
                    $after_distance += $card->xpath('str[@name="tra3_distance"]') ? (string)$card->xpath('str[@name="tra3_distance"]')[0] : 0;
                    $after_distance += $card->xpath('str[@name="tra4_distance"]') ? (string)$card->xpath('str[@name="tra4_distance"]')[0] : 0;
                    $after_distance += $card->xpath('str[@name="tra5_distance"]') ? (string)$card->xpath('str[@name="tra5_distance"]')[0] : 0;
                    $user->cards()->where('card_surface_id', $card_surface_id)->first()->update(['after_distance' => ceil($after_distance)]);

                }
            }
        }

//        dd($exchange_data);
        if ($exchange_data) {
            return $this->exchangeDice($exchange_data, $user);
        }

        return [];
    }


    private function shuffle_assoc($list)
    {
        if (!is_array($list)) return $list;

        $keys = array_keys($list);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $list[$key];
        }
        return $random;
    }

    private function exchangeDice($exchange_data, $user)
    {
        $result = [
            'tra1_reduction' => 0,
            'tra2_reduction' => 0,
            'tra3_reduction' => 0,
            'tra4_reduction' => 0,
            'tra5_reduction' => 0,
            'total_reduction' => 0
        ];
        $added_dices = 0;
        $mer_count = 0;
        $reduction_point = 0;
        $user_statistic = $user->statistic;
        $statistic_created_at = Carbon::parse($user_statistic->created_at);
        $statistic_updated_at = Carbon::parse($user_statistic->updated_at);
        $start_date = $statistic_updated_at->format('n/d');
        if ($statistic_created_at->day == $statistic_updated_at->day) {
            $start_date = '8/01';
        }

        if (isset($exchange_data['mer'])) {
            foreach ($exchange_data['mer'] as $key => $value) {
                $result[$key] = $value - $user_statistic->$key;
                $mer_count += $value - $user_statistic->$key;
                $user_statistic->$key = $value;
            }
        }
        if (isset($exchange_data['tra'])) {
            foreach ($exchange_data['tra'] as $key => $value) {
                $result[$key] = (int)((round($value, 3) - $user_statistic->$key) * 1000);
                $result['total_reduction'] += $result[$key];
                $reduction_point += ($value - $user_statistic->$key) * config('game.' . $key . '_weighted');
                $user_statistic->$key = round($value, 3);
            }
        }
        $added_dices += floor(($reduction_point + $user->remainder) / 0.3);
        $added_dices += $mer_count;
        $remainder = fmod(($reduction_point + $user->remainder), 0.3);
        if ($added_dices >= config('game.max_dices')) {
            $added_dices = config('game.max_dices');
        }
        if ($added_dices < 0) {
            $added_dices = 0;
            $remainder = 0;
        }
        $user_statistic->save();
        $user->addDice($added_dices);
        $user->update(['remainder' => $remainder]);

        $result['added_dices'] = $added_dices;
        $result['mer_count'] = $mer_count;
        $result['start_date'] = $start_date;
        $result['end_date'] = Carbon::now()->format('n/d');
        return $result;
    }

    public function createCardByPhysicalID($id)
    {
        $url = config('api.card_api_url');
        $client = new Client();
        $response = $client->request('GET', $url . '/GetDataByPhysicalID?cardID=' . $id,
            ['timeout' => 120]);
        $xml = simplexml_load_string(simplexml_load_string($response->getBody()->getContents()));
        $card_info = [
            'card_physical_id' => $xml->xpath('//str[@name="card_physical_id"]') ? (string)$xml->xpath('//str[@name="card_physical_id"]')[0] : 0,
            'card_surface_id' => $xml->xpath('//str[@name="card_surface_id"]') ? (string)$xml->xpath('//str[@name="card_surface_id"]')[0] : 0,
            'mer' => [
                'mer1_count' => $xml->xpath('//int[@name="mer1_count"]') ? (string)$xml->xpath('//int[@name="mer1_count"]')[0] : 0,
                'mer2_count' => $xml->xpath('//int[@name="mer2_count"]') ? (string)$xml->xpath('//int[@name="mer2_count"]')[0] : 0,
                'mer3_count' => $xml->xpath('//int[@name="mer3_count"]') ? (string)$xml->xpath('//int[@name="mer3_count"]')[0] : 0,
                'mer4_count' => $xml->xpath('//int[@name="mer4_count"]') ? (string)$xml->xpath('//int[@name="mer4_count"]')[0] : 0,
                'mer5_count' => $xml->xpath('//int[@name="mer5_count"]') ? (string)$xml->xpath('//int[@name="mer5_count"]')[0] : 0,
            ],
            'tra_reduction' => [
                'tra1_reduction' => $xml->xpath('//str[@name="tra1_reduction"]') ? (string)$xml->xpath('//str[@name="tra1_reduction"]')[0] : 0,
                'tra2_reduction' => $xml->xpath('//str[@name="tra2_reduction"]') ? (string)$xml->xpath('//str[@name="tra2_reduction"]')[0] : 0,
                'tra3_reduction' => $xml->xpath('//str[@name="tra3_reduction"]') ? (string)$xml->xpath('//str[@name="tra3_reduction"]')[0] : 0,
                'tra4_reduction' => $xml->xpath('//str[@name="tra4_reduction"]') ? (string)$xml->xpath('//str[@name="tra4_reduction"]')[0] : 0,
                'tra5_reduction' => $xml->xpath('//str[@name="tra5_reduction"]') ? (string)$xml->xpath('//str[@name="tra5_reduction"]')[0] : 0,
            ],
            'tra_distance' => [
                'tra1_distance' => $xml->xpath('//str[@name="tra1_distance"]') ? (string)$xml->xpath('//str[@name="tra1_distance"]')[0] : 0,
                'tra2_distance' => $xml->xpath('//str[@name="tra2_distance"]') ? (string)$xml->xpath('//str[@name="tra2_distance"]')[0] : 0,
                'tra3_distance' => $xml->xpath('//str[@name="tra3_distance"]') ? (string)$xml->xpath('//str[@name="tra3_distance"]')[0] : 0,
                'tra4_distance' => $xml->xpath('//str[@name="tra4_distance"]') ? (string)$xml->xpath('//str[@name="tra4_distance"]')[0] : 0,
                'tra5_distance' => $xml->xpath('//str[@name="tra5_distance"]') ? (string)$xml->xpath('//str[@name="tra5_distance"]')[0] : 0,
            ],
        ];
        if (!$card_info['card_physical_id']) {
            return false;
        }
        $tra_reduction = $this->shuffle_assoc($card_info['tra_reduction']);
        arsort($tra_reduction);

        $card = Card::create([
            'card_surface_id' => $card_info['card_surface_id'],
            'card_physical_id' => $card_info['card_physical_id'],
            'before_data' => json_encode($card_info),
            'before_first_tra' => array_keys($tra_reduction)[0],
            'before_second_tra' => array_keys($tra_reduction)[1],
            'before_reduction' => round(array_sum($tra_reduction), 3),
            'before_distance' => ceil(array_sum($card_info['tra_distance'])),
            'before_count' => array_sum($card_info['mer']),
        ]);

        $name = '';
//        $type = null;
        $arr = [
            'tra1_reduction' => 5,
            'tra2_reduction' => 1,
            'tra3_reduction' => 0,
            'tra4_reduction' => 2,
            'tra5_reduction' => 3,
        ];
        if ($card->before_reduction == 0) {
            $curvesType = 4;
            $color = 4;
        } else {
            $curvesType = $arr[$card->before_second_tra];
            $color = $arr[$card->before_first_tra];
        }
        if ($curvesType == 3) {
            $bendingDegree = 0;
        } else {
            $bendingDegree = mt_rand(-15, 15);
        }

        if ($curvesType == 0) {
            $bendingSmooth = mt_rand(1, 2) / 10;
        } elseif ($curvesType == 1) {
            $bendingSmooth = 0.1;
        } else {
            $bendingSmooth = mt_rand(1, 20) / 10;
        }
        $angle = mt_rand(-2, 6) / 10;
        $angleBias = mt_rand(7, 125) / 10;
        $flex = mt_rand(-50, 15) / 100;
        $leafType = mt_rand(0, 4);
        $petalType = mt_rand(0, 9);
        $arr2 = [
            'tra1_reduction' => [3, 4, 5],
            'tra2_reduction' => [6, 7, 8],
            'tra3_reduction' => [12, 13, 14],
            'tra4_reduction' => [0, 1, 2],
            'tra5_reduction' => [9, 10, 11],
            'mer1_count' => [27, 28, 29],
            'mer2_count' => [18, 19, 20],
            'mer3_count' => [15, 16, 17],
            'mer4_count' => [21, 22, 23],
            'mer5_count' => [24, 25, 26],
        ];
        $name_arr = [
            'tra1_reduction' => ['卍霸', '距向系', '刺球'],
            'tra2_reduction' => ['宿命√', '跨域系', '過山'],
            'tra3_reduction' => ['煞氣a', '時間系', '地下'],
            'tra4_reduction' => ['夢☆', '角力系', '魔豆'],
            'tra5_reduction' => ['Baby', '強化系', '閃蝶'],
            'mer1_count' => ['正義', '火焰系'],
            'mer2_count' => ['戀羽', '變化系'],
            'mer3_count' => ['天下', '特質系'],
            'mer4_count' => ['永恆', '原生系'],
            'mer5_count' => ['琥珀', '操作系'],
            'end' => ['花', '仙', '蕨']
        ];
        if ($card->before_reduction == 0) {
            $data = json_decode($card->before_data);
            $mer = $this->shuffle_assoc($data['mer']);
            arsort($mer);
            $result = $arr2[array_keys($mer)[0]];
            $texture = $result[mt_rand(0, 2)];
            $name .= $name_arr[array_keys($mer)[0]][0];
//            $type = $name_arr[array_keys($mer)[0]][1];
        } else {
            $result = $arr2[$card->before_first_tra];
            $texture = $result[mt_rand(0, 2)];
            $name .= $name_arr[$card->before_first_tra][0];
//            $type = $name_arr[$card->before_first_tra][1];
        }
        $name .= $name_arr[$card->before_second_tra][2];
        $name .= $name_arr['end'][mt_rand(0, 2)];
        $textureRepeat = mt_rand(15, 35) / 10;
        $stamenType = mt_rand(0, 9);
        $stamenColor = mt_rand(0, 4);

        $plant_data = json_encode([
            'curvesType' => $curvesType,
            'color' => $color,
            'bendingDegree' => $bendingDegree,
            'bendingSmooth' => $bendingSmooth,
            'angle' => $angle,
            'angleBias' => $angleBias,
            'flex' => $flex,
            'leafType' => $leafType,
            'petalType' => $petalType,
            'texture' => $texture,
            'textureRepeat' => $textureRepeat,
            'stamenType' => $stamenType,
            'stamenColor' => $stamenColor,
        ]);

        $card->update([
            'plant_data' => $plant_data,
            'name' => $name,
//            'type' => $type,
        ]);

        return $card;
    }

}
