<?php

namespace App\Supports;

use Illuminate\Pagination\LengthAwarePaginator;

class Response
{
    /**
     *    response ok
     *    @param  [type]   $result         [description]
     *    @param  array    $custom         [description]
     *    @param  integer  $responseCode   [description]
     *    @return object                   [description]
     */
    public static function ok($result = [], int $responseCode = 200, array $custom = [])
    {
        $paginator = [];
        if ($result instanceof LengthAwarePaginator) {
            $paginator = collect($result->toArray());
            $result = $paginator->pull('data');
            $paginator = ['paginate' => $paginator];
        }

        $output = array_merge(['data' => $result], $paginator, $custom);

        return response()->json($output, $responseCode);
    }

    /**
     *    response fail
     *    @param  string   $message        [description]
     *    @param  array    $payload        [description]
     *    @param  integer  $responseCode   [description]
     *    @return object                   [description]
     */
    public static function fail($message = 'api error', int $responseCode = 400, array $payload = [])
    {
        return response()->json([
            'message' => $message,
            'errors'  => $payload,
        ], $responseCode);
    }
}
