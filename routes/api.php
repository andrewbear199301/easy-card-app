<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [\App\Http\Controllers\UserController::class, 'login']);
Route::post('card', [\App\Http\Controllers\CardController::class, 'store']);

Route::post('exhibition/card', [\App\Http\Controllers\CardController::class, 'storeByPhysical']);

Route::get('test', [\App\Http\Controllers\UserController::class, 'test']);
//Route::get('test', function (Request $request) {
//    return $request->ip();
//});

Route::group(['middleware' => ['auth:sanctum']], function () {
    //會員
    Route::get('user', [\App\Http\Controllers\UserController::class, 'index']);
//    Route::get('user/check', [\App\Http\Controllers\UserController::class, 'check']);
    Route::put('user/name', [\App\Http\Controllers\UserController::class, 'updateName']);

    //植物
    Route::get('card/list', [\App\Http\Controllers\CardController::class, 'list']);
    Route::put('card/name', [\App\Http\Controllers\CardController::class, 'updateName']);
    Route::put('card/map', [\App\Http\Controllers\CardController::class, 'changeMapCard']);

    //骰子
    Route::get('dice/roll', [\App\Http\Controllers\DiceController::class, 'roll']);
//    Route::get('test', [\App\Http\Controllers\DiceController::class, 'test']);

    //回答問題
    Route::post('quiz/answer', [\App\Http\Controllers\QuizController::class, 'answer']);

    //每日任務
    Route::get('daily', [\App\Http\Controllers\DailyController::class, 'index']);
    Route::get('daily/exchange', [\App\Http\Controllers\DailyController::class, 'exchangeDaily']);
    Route::post('achievement/exchange', [\App\Http\Controllers\DailyController::class, 'exchangeAchievement']);
    Route::get('birthday', [\App\Http\Controllers\DailyController::class, 'birthday']);

    //回饋券
    Route::get('reward', [\App\Http\Controllers\RewardController::class, 'index']);

    //減碳表
    Route::get('reduction', [\App\Http\Controllers\ReductionController::class, 'index']);

    //leaderboard
    Route::get('leaderboard', [\App\Http\Controllers\LeaderboardController::class, 'index']);

    //存圖
//    Route::post('save_image', [\App\Http\Controllers\LeaderboardController::class, 'saveImage']);
});


