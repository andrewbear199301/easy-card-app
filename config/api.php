<?php

return [
    'card_api_url' => env('CARD_API_URL', 'http://211.78.134.173:7071/carbon.asmx'),
    'token_api_url' => env('TOKEN_API_URL', 'http://211.78.134.173:7071/carbon.asmx'),
    'reward_api_url' => env('REWARD_API_URL', 'https://dgct.easycard.com.tw/ICERWebAPI/api/converters'),
    'encrypt_key' => env('ENCRYPT_KEY', '9e1q9Gn0Vbp7EVh0Ps02ImE09d6P8001'),
];
