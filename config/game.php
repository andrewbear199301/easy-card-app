<?php

return [
    'map' => [
        1 => [
            'type' => 5,
            'content' => '悠遊20週年，有你加入，前往低碳生活更進一步！',
        ],
        2 => [
            'type' => 4,
            'content' => '',
        ],
        3 => [
            'type' => 1,
            'content' => '',
        ],
        4 => [
            'type' => 4,
            'content' => '',
        ],
        5 => [
            'type' => 5,
            'content' => '有些圖書館可以用悠遊卡借書！',
        ],
        6 => [
            'type' => 1,
            'content' => '',
        ],
        7 => [
            'type' => 2,
            'content' => '',
        ],
        8 => [
            'type' => 6,
            'content' => '川貝母',
            'artist_id' => 'artist_1',
            'sub_id' => 1,
        ],
        9 => [
            'type' => 5,
            'content' => '醫藥費可以使用悠遊卡靠卡付款呦～',
        ],
        10 => [
            'type' => 3,
            'content' => '誠品',
            'reward_id' => 9,
        ],
        11 => [
            'type' => 2,
            'content' => '',
        ],
        12 => [
            'type' => 3,
            'content' => '統一超商',
            'reward_id' => 1,
        ],
        13 => [
            'type' => 4,
            'content' => '',
        ],
        14 => [
            'type' => 3,
            'content' => 'CoCo都可',
            'reward_id' => 6,
        ],
        15 => [
            'type' => 6,
            'content' => 'houth',
            'artist_id' => 'artist_2',
            'sub_id' => 2,
        ],
        16 => [
            'type' => 1,
            'content' => '',
        ],
        17 => [
            'type' => 5,
            'content' => '越來越多廟宇可以透過悠遊卡捐款與點燈呢！',
        ],
        18 => [
            'type' => 4,
            'content' => '',
        ],
        19 => [
            'type' => 2,
            'content' => '',
        ],
        20 => [
            'type' => 6,
            'content' => '陳普',
            'artist_id' => 'artist_3',
            'sub_id' => 3,
        ],
        21 => [
            'type' => 3,
            'content' => '鬍鬚張',
            'reward_id' => 11,
        ],
        22 => [
            'type' => 5,
            'content' => '遊樂園設施可以使用悠遊卡扣款搭乘好方便！',
        ],
        23 => [
            'type' => 1,
            'content' => '',
        ],
        24 => [
            'type' => 6,
            'content' => '低級失誤',
            'artist_id' => 'artist_4',
            'sub_id' => 4,
        ],
        25 => [
            'type' => 3,
            'content' => '85度C',
            'reward_id' => 5,
        ],
        26 => [
            'type' => 1,
            'content' => '',
        ],
        27 => [
            'type' => 5,
            'content' => '搭船可以嗶悠遊卡好方便！',
        ],
        28 => [
            'type' => 4,
            'content' => '',
        ],
        29 => [
            'type' => 6,
            'content' => 'XAP',
            'artist_id' => 'artist_5',
            'sub_id' => 5,
        ],
        30 => [
            'type' => 3,
            'content' => '全家便利商店',
            'reward_id' => 2,
        ],
        31 => [
            'type' => 4,
            'content' => '',
        ],
        32 => [
            'type' => 6,
            'content' => '陳又凌',
            'artist_id' => 'artist_6',
            'sub_id' => 6,
        ],
        33 => [
            'type' => 2,
            'content' => '',
        ],
        34 => [
            'type' => 4,
            'content' => '',
        ],
        35 => [
            'type' => 1,
            'content' => '',
        ],
        36 => [
            'type' => 2,
            'content' => '',
        ],
        37 => [
            'type' => 5,
            'content' => '許多餐飲店可以使用悠遊付支付！',
        ],
        38 => [
            'type' => 6,
            'content' => 'An Chen',
            'artist_id' => 'artist_7',
            'sub_id' => 7,
        ],
        39 => [
            'type' => 2,
            'content' => '',
        ],
        40 => [
            'type' => 6,
            'content' => 'Likuan Zhen',
            'artist_id' => 'artist_8',
            'sub_id' => 8,
        ],
        41 => [
            'type' => 1,
            'content' => '',
        ],
        42 => [
            'type' => 2,
            'content' => '',
        ],
        43 => [
            'type' => 3,
            'content' => '萊爾富',
            'reward_id' => 3,
        ],
        44 => [
            'type' => 4,
            'content' => '',
        ],
        45 => [
            'type' => 1,
            'content' => '',
        ],
        46 => [
            'type' => 2,
            'content' => '',
        ],
        47 => [
            'type' => 4,
            'content' => '',
        ],
        48 => [
            'type' => 5,
            'content' => '沒有零錢投幣，嗶卡就可以來一杯涼爽飲料～！',
        ],
        49 => [
            'type' => 4,
            'content' => '',
        ],
        50 => [
            'type' => 3,
            'content' => 'ＯＫ便利商店',
            'reward_id' => 4,
        ],
        51 => [
            'type' => 1,
            'content' => '',
        ],
        52 => [
            'type' => 6,
            'content' => '星期一回收日',
            'artist_id' => 'artist_9',
            'sub_id' => 9,
        ],
        53 => [
            'type' => 3,
            'content' => '美廉社',
            'reward_id' => 8,
        ],
        54 => [
            'type' => 2,
            'content' => '',
        ],
        55 => [
            'type' => 1,
            'content' => '',
        ],
        56 => [
            'type' => 3,
            'content' => '三商巧福',
            'reward_id' => 10,
        ],
        57 => [
            'type' => 5,
            'content' => '野柳女王頭門票可以用悠遊卡付款呦！',
        ],
        58 => [
            'type' => 6,
            'content' => 'Jing D Wang',
            'artist_id' => 'artist_10',
            'sub_id' => 10,
        ],
        59 => [
            'type' => 3,
            'content' => '清心',
            'reward_id' => 7,
        ],
        60 => [
            'type' => 5,
            'content' => '一卡悠遊，環島樂悠悠～',
        ],
    ],

    'quizzes' => [
        1 => [
            'id' => 1,
            'title' => '悠遊卡的有效期限？',
            'answer' => 3,
            'choices' => [
                1 => '5年',
                2 => '10年',
                3 => '20年',
            ]
        ],
        2 => [
            'id' => 2,
            'title' => '悠遊卡有幾種身份別卡？',
            'answer' => 2,
            'choices' => [
                1 => '4款',
                2 => '6款',
                3 => '10款',
            ]
        ],
        3 => [
            'id' => 3,
            'title' => '最貴的造型悠遊卡官方發售價？',
            'answer' => 3,
            'choices' => [
                1 => '399元',
                2 => '899元',
                3 => '1799元',
            ]
        ],
        4 => [
            'id' => 4,
            'title' => '哪一位奧運選手有出過悠遊卡？',
            'answer' => 3,
            'choices' => [
                1 => '楊勇緯',
                2 => '郭婞淳',
                3 => '李洋、王齊麟',
            ]
        ],
        5 => [
            'id' => 5,
            'title' => '悠遊卡吉祥物BeBe是哪一種吉祥物？',
            'answer' => 2,
            'choices' => [
                1 => '狗',
                2 => '倉鼠',
                3 => '樹懶',
            ]
        ],
        6 => [
            'id' => 6,
            'title' => '每人平均擁有的悠遊卡數量？',
            'answer' => 2,
            'choices' => [
                1 => '2張',
                2 => '4張',
                3 => '7張',
            ]
        ],
        7 => [
            'id' => 7,
            'title' => '悠遊卡感應扣款的交易時間？',
            'answer' => 1,
            'choices' => [
                1 => '0.4秒',
                2 => '0.7秒',
                3 => '1秒',
            ]
        ],
        8 => [
            'id' => 8,
            'title' => '20年來使用悠遊卡搭大眾運輸相對於汽車的減碳量？',
            'answer' => 2,
            'choices' => [
                1 => '約342萬公噸',
                2 => '約824萬公噸',
                3 => '約5000萬公噸',
            ]
        ],
        9 => [
            'id' => 9,
            'title' => '20年來使用悠遊卡搭車的距離可繞地球幾圈？',
            'answer' => 2,
            'choices' => [
                1 => '200萬圈',
                2 => '354萬圈',
                3 => '1億圈',
            ]
        ],
        10 => [
            'id' => 10,
            'title' => '20年來悠遊卡陪伴大家搭過幾趟車？',
            'answer' => 3,
            'choices' => [
                1 => '約1000萬趟',
                2 => '約200億趟',
                3 => '約300億趟',
            ]
        ]
    ],


    'chance_tools' => [
        1 => [
            'title' => '一級能源',
            'message' => '獲得一星級Be能源，★能量+2',
            'point' => 2,
        ],
        2 => [
            'title' => '三級能源',
            'message' => '獲得三星級Be能源，★能量+3',
            'point' => 3,
        ],
        3 => [
            'title' => '五級能源',
            'message' => '獲得五星級Be能源，★能量+5',
            'point' => 5,
        ],
    ],

    'tools' => [
        1 => [
            'title' => '一級能源',
            'message' => '幫植物嗶能源，★能量+2',
            'point' => 2,
        ],
        2 => [
            'title' => '三級能源',
            'message' => '幫植物嗶能源，★能量+3',
            'point' => 3,
        ],
        3 => [
            'title' => '五級能源',
            'message' => '幫植物嗶能源，★能量+5',
            'point' => 5,
        ],
    ],


    'max_dices' => env('MAX_DICES', 15),

    'chance_1' => env('CHANCE_1', 0.1),
    'chance_2' => env('CHANCE_2', 0.2),
    'chance_3' => env('CHANCE_3', 0.3),
    'chance_4' => env('CHANCE_4', 0.4),
    'chance_5' => env('CHANCE_5', 0.5),
    'chance_6' => env('CHANCE_6', 0.6),
    'finish_lap' => env('FINISH_LAP', 0.5),
    'enterprise' => env('ENTERPRISE', 0.5),
    'tra1_reduction_weighted' => env('TRA1_REDUCTION_WEIGHTED', 1.2),
    'tra2_reduction_weighted' => env('TRA2_REDUCTION_WEIGHTED', 0.2),
    'tra3_reduction_weighted' => env('TRA3_REDUCTION_WEIGHTED', 1),
    'tra4_reduction_weighted' => env('TRA4_REDUCTION_WEIGHTED', 5),
    'tra5_reduction_weighted' => env('TRA5_REDUCTION_WEIGHTED', 0.1),

    'card_name' => [
        ''
    ]
];
